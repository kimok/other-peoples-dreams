(ns hotreload
  (:require ashaw.op-dreams
            [nrepl.server :as nrepl]
            [ring.adapter.jetty :refer [run-jetty]]
            [ring.middleware.reload :refer [wrap-reload]]
            [cider.nrepl :refer [cider-middleware]]))

(def dev-handler (wrap-reload #'ashaw.op-dreams/handler))
(defn -main []
  (run-jetty dev-handler {:port 8818 :join? false})
  (nrepl/start-server
   :port 7000
   :bind "0.0.0.0"
   :middleware [cider-middleware]))
