* Other People's Dreams
A piece of interactive art for the web.

** Artist's Statement
Two tapestries, handwoven into digital code. One to cover, one to float above.
The code, a shifting and archival consciousness.
The writing, a work in which the words are never mine.
The performance, an experiment. To sleep beneath the auspices of other people’s dreams.
Wanderings to filter through the interstice of sleep.
My sleep, the warp.
Your words, the weft.
The fabric, our collective humanity.

** Attribution
- Alix Anne Shaw  :: Artist
- Kimo Knowles    :: Web Designer
- Stephanie Gomez :: Assistant

** Local development
1) Get the repository on your local machine:

   ~git clone https://gitlab.com/kimok/other-peoples-dreams~

   ~cd other-peoples-dreams~

2) Install local javascript dependencies:

   ~npm install~

   ([[https://docs.npmjs.com/downloading-and-installing-node-js-and-npm][don't have npm?]])

3) Start the backend webserver:

   ~docker compose -f dev.docker-compose.yml up~

   ([[https://docs.docker.com/compose/install/][don't have docker compose?]])
4) Start the clojurescript compiler:

   ~npx shadow-cljs watch dev~

   Note: Make sure to use a new, separate terminal for this.
   In other words, you need 2 terminals to run both steps 3 & 4.

5) Visit the local site:
   In a web browser, visit ~localhost:8818~
   Refresh the page if it doesn't load immediately.

** Deployment
This project configures [[https://docs.gitlab.com/ee/ci/][GitLab CI]] to deploy to a [[https://www.debian.org/][debian]] webserver.

1) Procure a debian server.
2) Register a [[https://en.wikipedia.org/wiki/List_of_DNS_record_types][DNS A record]].
   - It should direct the domain otherpeoplesdreams.net to the debian server IP.
   - To use a different domain, change [[https://gitlab.com/kimok/other-peoples-dreams/blob/master/docker-compose.yml#L15][VIRTUAL_HOST]] and [[https://gitlab.com/kimok/other-peoples-dreams/blob/master/docker-compose.yml#L15][LETSENCRYPT_HOST]].
3) (optional) Mount a postgres database inside to the /mnt/database directory on the debian server.
   - For instance, [[https://docs.digitalocean.com/products/volumes/][digitalocean block storage]] can be used.
   - Postgres will look for a database at /mnt/database/pgdata.
   - If it doesn't exist (e.g., if nothing is mounted), it should create a fresh database there.
     This is probably okay, but it's untested.
4) Generate an [[https://en.wikipedia.org/wiki/Secure_Shell][SSH keypair]].
5) [[https://www.digitalocean.com/community/tutorials/how-to-configure-ssh-key-based-authentication-on-a-linux-server#step-2-copying-an-ssh-public-key-to-your-server][Install the SSH public key]] on the debian server.
6) Set [[https://docs.gitlab.com/ee/ci/variables/index.html][variables]] on the gitlab repository:
   - $PROD_SERVER_IP   :: The ipv4 address of the debian server.
   - $PROD_SERVER_KEY  :: An OpenSSH private key.
     - A [[https://docs.gitlab.com/ee/ci/variables/index.html#use-file-type-cicd-variables][file-type]] variable.
     - Must match the public key installed on the debian server.
   - $PROD_SERVER_USER :: The name of the [[https://wiki.debian.org/UserAccounts][debian user]] on the debian server.
     - A value of root should work.
     - It could be more secure to create a new user instead of using root.
7) [[https://git-scm.com/docs/git-push][Push]] to the [[https://docs.gitlab.com/ee/user/project/repository/branches/default.html][default branch]] to [[https://gitlab.com/kimok/other-peoples-dreams/blob/master/.gitlab-ci.yml#L16][trigger a deployment]].
   - The [[https://github.com/nginx-proxy/acme-companion?tab=readme-ov-file#requirements][acme companion]] should automatically set up an SSL certificate via [[https://letsencrypt.org/how-it-works/][letsencrypt]].
   - After a few minutes, https://otherpeopesdreams.net should be publicly available.
