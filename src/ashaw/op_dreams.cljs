(ns ashaw.op-dreams
  (:require [reagent.core :as r]
            [reagent.dom :as rdom]
            [reagent.ratom :refer [reaction]]
            [clojure.walk :refer [keywordize-keys]]
            [cljs.core.async :refer [<! timeout go go-loop]]
            [ajax.core :as ajax]))

(defonce state (r/atom {:which-dream 0
                        :frame :habibula
                        :rand-xy [0 0]
                        :dreams {}
                        :background-color "white"
                        :brightness 1}))

(defn check-viewport [] (swap! state assoc :mobile-layout? (<= js/window.innerWidth 1023)))

(defonce _ (js/window.addEventListener "resize" check-viewport))

(check-viewport)

(defn thread [s]
  (let [id (rand)
        fade (reaction (if (get-in @state [:fade id]) 1 0))]
    (r/create-class
     {:component-did-mount (fn [_] (go (<! (timeout 16)) (swap! state assoc-in [:fade id] 1)))
      :reagent-render (fn [s] [:span {:style {:transition "opacity 3s, filter 2s"
                                              :opacity @fade
                                              :filter (str "blur(" (* 5 (- 1 @fade)) "px)")}} s])})))

(def the-dream
  (reaction (let [{:keys [which-dream dreams]} @state]
              (get dreams which-dream))))

(defonce tick (go-loop []
                (swap! state update :seconds inc)
                (swap! state assoc :rand-xy [(rand) (rand)])
                (<! (timeout 1000))
                (recur)))

(defn split-sentences [s]
  (or (re-seq #"[^\s][^\.]+[\.\?]" s) [s]))

(defn got-dream [m]
  (let [{old-id :which-dream} @state
        {:keys [id dream]} (keywordize-keys m)
        new-id (if (not= old-id id)
                 id
                 (rand-nth
                  (seq
                   (clojure.set/difference
                    (set (keys (:dreams @state)))
                    #{old-id}))))]
    (swap! state update :dreams assoc id dream)
    (swap! state assoc :which-dream new-id)))

(defn get-dream [& [after]]
  (ajax/GET
    "/dream"
    {:handler #(do (got-dream %)
                   (when after (after)))
     :error-handler js/console.log}))

(defn darken! [& [brightness]]
  (let [new-brightness (or brightness
                           (max 0 (-> @state :brightness (- 0.1))))]
    (swap! state assoc :brightness new-brightness)))

(def text-color (reaction
                 (case (:frame @state)
                   :remember "white"
                   (:habibula :weave)
                   (if (< (:brightness @state) 0.5) "white" "black")
                   "black")))

(defn weave-sentence! []
  (when-let [next-sent (first (:woven-dream-sentences @state))]
    (swap! state update :woven-dream-sentences rest)
    (swap! state update :woven-dream conj next-sent)
    (when-let [el (js/document.getElementById "weave")]
      (go (<! (timeout 16))
          (set! (.-scrollTop el) (.-scrollHeight el))))))

(defn weave! []
  (swap! state dissoc :woven-dream)
  (swap! state assoc :woven-dream-sentences
         (->> @state :dreams vals shuffle
              (take 2)
              (map split-sentences)
              (apply interleave)))
  (weave-sentence!))

(defn prompt! [{:keys [message speed force?]}]
  (go
    (<! (timeout 100))
    (let [mess message]
      (when-let [el (js/document.getElementById "tabula")]
        (.focus el)
        (loop [i 0]
          (when (and (<= i (count mess))
                     (or force?
                         (not (:tabula @state))))
            (set! (.-value el) (subs mess 0 i))
            (<! (timeout (or speed 80)))
            (recur (inc i))))))))

(defn dream! [] (go (swap! state dissoc :fade)
                    (when @the-dream (<! (timeout 3000)))
                    (get-dream)
                    (darken!)))

(defn go-to [k]
  (case k
    :habibula (do (darken! 1)
                  (swap! state dissoc :fade)
                  (swap! state assoc
                         :dreams {}
                         :background-color "white"))
    :weave (do (swap! state dissoc :woven-dream :fade)
               (swap! state assoc :background-color "white")
               (get-dream #(get-dream weave!)))
    :remember (do (darken! 1)
                  (swap! state assoc :frame :remember
                         :background-color "midnightblue")
                  (swap! state dissoc :tabula)
                  (prompt! {:message "In my dream... "}))
    nil)
  (swap! state assoc :frame k))

(def forget-prompt! #(when-not (:tabula @state)
                       (when-let [el (js/document.getElementById "tabula")]
                         (set! (.-value el) (last (.-value el)))
                         (swap! state assoc :tabula true))))

(defn boop [{:keys [surface-structure deep-structure on-boop dream-destination shadow?]}]
  (let [{:keys [hover rand-xy]} @state
        hover! #(swap! state update :hover assoc surface-structure %)]
    [:div {:style {:position "relative"
                   :text-align "center" :flex 1
                   :overflow-x "visible"}
           :on-click #(do (on-boop) (hover! false))
           :on-mouse-enter #(hover! true)
           :on-mouse-leave #(hover! false)}
     [:a {:href (or dream-destination "#")
          :style {:overflow-x "visible"
                  :overflow-y "visible"
                  :text-decoration "none"
                  :color @text-color
                  :font-family "Arial, sans-serif"}}
      surface-structure]
     (when (not= false shadow?)
       [:div {:style {:pointer-events "none"
                      :position "absolute"
                      :height 0
                      :left 0
                      :z-index 9
                      :user-select "none"
                      :color "rgba(0,0,0,0)"
                      :text-shadow (str (first rand-xy)
                                        "rem "
                                        (second rand-xy)
                                        "rem 2.5px rgba(0,0,0,"
                                        (if (get hover surface-structure) 1 0)
                                        ")")
                      :transition "text-shadow 4s, transform 4s"
                      :transform "scale(2.5)"
                      :cursor "default"}}
        deep-structure])]))

(defn mobile-habibula []
  [:div {:style {:display "flex"
                 :flex-direction "column"
                 :height "100%"
                 :justify-content "space-between"}}
   [:div {:style {:text-align "center"
                  :font-size 40
                  :margin "40px"
                  :flex-basis "75%"
                  :min-height "15rem"
                  :overflow-y "scroll"}}
    ^{:key (rand)}
    [thread (or @the-dream "")]]
   [:div {:style {:display "flex"
                  :margin-top 40
                  :flex-direction "column"
                  :height 200
                  :font-size 50}}
    [boop {:surface-structure "Sleep"
           :deep-structure "D r e a m."
           :on-boop dream!}]
    [boop {:surface-structure "Submit"
           :deep-structure "Your Dream."
           :on-boop #(go-to :remember)}]
    [boop {:surface-structure "Weave"
           :deep-structure "Other   People's    Dreams"
           :on-boop #(go-to :weave)}]]])

(defn habibula []
  [:<>
   [:div {:style {:height "50%"
                  :padding "0 5rem 0 5rem"
                  :text-align "center"
                  :overflow-y "scroll"}}
    ^{:key (rand)}
    [thread (or @the-dream "")]]
   [:div {:style {:display "flex"
                  :justify-content "space-between"
                  :margin-top "3rem"}}
    [boop {:surface-structure "Sleep"
           :deep-structure "D r e a m."
           :on-boop dream!}]
    [boop {:surface-structure "Submit"
           :deep-structure "Your Dream."
           :on-boop #(go-to :remember)}]
    [boop {:surface-structure "Weave"
           :deep-structure "Other   People's    Dreams"
           :on-boop #(go-to :weave)}]]])

(defn submit-dream [dream & [handler]]
  (ajax/POST
    "/dream"
    {:handler (or handler identity)
     :error-handler js/console.log
     :params dream}))

(defn grate! []
  (go
    (swap! state assoc :background-color "black" :color "white" :grating? true)
    (prompt! {:message "Thank you for adding to the dreamscape." :speed 70 :force? true})
    (<! (timeout 4500))
    (go-to :habibula)
    (swap! state assoc :grating? false)))

(defn tabula []
  [:div {:style {:display "flex"
                  :flex-direction "column"
                  :height "100%"
                  :justify-content "space-between"}}
    [:textarea#tabula {:auto-focus true
                       :spellcheck "false"
                       :on-input forget-prompt!
                       :style {:color @text-color
                               :border "none"
                               :background "transparent"
                               :resize "none"
                               :outline "none"
                               :font-size 40
                               :margin 0
                               :width "100%"
                               :height "60%"
                               :text-align "center"}}]
    [:div {:style {:margin-top "4rem"
                   :font-size (when (:mobile-layout? @state) 50)
                   :display (when-not (:mobile-layout? @state) "flex")}}
     (when-not (:grating? @state)
       [:<>
        [boop {:surface-structure "Submit"
               :deep-structure "S u b m i t"
               :on-boop #(when-let [el (.getElementById js/document "tabula")]
                           (submit-dream {:dream (.-value el)}
                                         (fn [] (grate!))))}]
        [boop {:surface-structure "Awaken"
               :deep-structure "A w a k e n."
               :on-boop #(go-to :habibula)}]])]])

(defn mobile-tabula []
  [:div {:style {:margin 40
                 :height "80%"
                 :font-size 60}}
   [tabula]])

(defn get-unremembereds! []
  (ajax.core/GET
    "/unremembereds"
    {:handler (fn [m]
                (let [v (:unremembereds (keywordize-keys m))]
                  (swap! state assoc :unremembereds
                         (into {}
                               (map #(do [(:id %) %]))
                               v))))
     :error-handler js/console.log}))

(defn forgot! [] (get-unremembereds!))

(defn forget [{:keys [id] :as dream}]
  (when id
    (ajax/POST
      "/forget"
      {:handler forgot!
       :error-handler js/console.log
       :params dream})))

(defn logged-in! [{:keys [authenticated?] :as res}]
  (when (get res "authenticated?")
    (swap! state assoc :frame :admin)
    (get-unremembereds!)))

(defn log-in! []
  (let [password (some-> "password"
                         js/document.getElementById
                         .-value)]
    (ajax/POST
      "/admin"
      {:handler logged-in!
       :error-handler js/console.log
       :params {:password password}})))

(defn login []
  [:<>
   [:input#password {:type "password"}]
   [:button {:on-click log-in!} "log in"]])

(defn debounce [f interval]
  (let [t (atom nil)]
    (fn [& args]
      (when @t (js/clearTimeout @t))
      (reset! t (js/setTimeout #(apply f args) interval)))))

(defn dream-editor [{:keys [dream id]}]
  (let [path [:unremembereds id]
        submit #(submit-dream (get-in @state path))
        debounce-submit (debounce submit 1000)
        check (fn [k] #(do (swap! state update-in (conj path k) not)
                           (submit)))]
    (fn [{:keys [dream id]}]
      (let [{:keys [shared nightmare recurring id]} (get-in @state path)]
        [:div {:style {:margin "5"
                       :padding "5"
                       :background "lightblue"
                       :display "flex"}}
         [:textarea {:style {:flex 1}
                     :value dream
                     :on-change
                     #(do (swap! state assoc-in
                                 [:unremembereds id :dream]
                                 (.-value (.-target %)))
                          (debounce-submit))}]
         [:div {:style {:display "flex"
                        :flex-direction "column"}}
          [:label "public?"]
          [:input {:type "checkbox"
                   :checked shared
                   :on-change (check :shared)}]
          [:button {:on-click #(forget (get-in @state [:unremembereds id]))} "delete"]]
         [:div {:style {:display "flex"
                        :flex-direction "column"}}
          [:label "nightmare"]
          [:input {:type "checkbox"
                   :checked nightmare
                   :on-change (check :nightmare)}]
          [:label "recurring"]
          [:input {:type "checkbox"
                   :checked recurring
                   :on-change (check :recurring)}]]]))))

(defn admin []
  (if-let [dreams (vals (:unremembereds @state))]
    (->> dreams
         (sort-by :shared)
         (map #(do ^{:key (:id %)} [dream-editor %]))
         (into [:div {:style {:overflow-y "scroll"
                              :height "75%"}}]))
    "Can't find any dreams."))

(def woven-dream (reaction (->> @state
                                :woven-dream
                                reverse
                                (interpose " ")
                                (map #(do [thread %])))))

(defn mobile-real-weave []
  [:div {:style {:display "flex"
                 :flex-direction "column"
                 :height "100%"
                 :justify-content "space-between"}}
   [:div#weave {:style {:font-size 40
                        :margin "40px"
                        :flex-basis "70%"
                        :overflow-y "scroll"}}
    @woven-dream]
   [:div {:style {:display "flex"
                  :margin-top 30
                  :flex-direction "column"
                  :height 200
                  :font-size 50}}
    (when (seq (:woven-dream-sentences @state))
      [boop {:surface-structure "Keep Weaving"
             :deep-structure "W e a v e."
             :on-boop weave-sentence!}])
    [boop {:surface-structure "Awaken"
           :deep-structure "A w a k e n."
           :on-boop #(go-to :habibula)}]]])

(defn real-weave []
  [:div
   [:div#weave {:style {:height "50%"
                        :padding "0 5rem 0 5rem"
                        :overflow-y "scroll"}}
    @woven-dream]
   [:div {:style {:display "flex"
                  :margin-top "3rem"}}
    (when (seq (:woven-dream-sentences @state))
      [boop {:surface-structure "Keep Weaving"
             :deep-structure "W e a v e."
             :on-boop weave-sentence!}])
    [boop {:surface-structure "Awaken"
           :deep-structure "A w a k e n."
           :on-boop #(go-to :habibula)}]]])

(def show-bed? (reaction (-> @state :frame #{:habibula :weave})))

(defn router []
  (if (:mobile-layout? @state)
    (case (:frame @state)
      :remember [mobile-tabula]
      :login [login]
      :admin [admin]
      :weave [mobile-real-weave]
      [mobile-habibula])
    (case (:frame @state)
      :remember [tabula]
      :login [login]
      :admin [admin]
      :weave [real-weave]
      [habibula])))

(def close-modal! #(swap! state dissoc :modal?))
(def open-modal! #(swap! state assoc :modal? true))

(defn notify! []
  (ajax/POST
   "/notify"
   {:handler       js/console.log
    :error-handler js/console.log}))

(defn attribution []
  [:div {:on-click notify!
         :style {:position "absolute"
                 :bottom 10
                 :right 0
                 :writing-mode "sideways-lr"
                 :opacity "20%"
                 :font-size (if (:mobile-layout? @state) 16 10)
                 :font-family "mono"}}
   [:a {:href "#"
        :style {:display "block"
                :color @text-color
                :text-decoration "none"
                :margin 0
                :padding 0}}
    "© Alix Anne Shaw"
    [:br]
    "λ Kimo Knowles"]])

(defn header []
  [:div {:style {:pointer-events "none"
                 :display "flex"
                 :opacity (if @show-bed? 1 0)
                 :text-align "center"
                 :transition "filter 3s, opacity 6s"
                 :filter (str "brightness(" (:brightness @state) ")")}}
   [:img {:src "alix-tapestry-clean 2.png"
          :width "100%"}]])

(defn footer []
  [:<>
   [:div {:style {:pointer-events "none"
                  :margin-left "-30%"
                  :opacity (if @show-bed? 1 0)
                  :transition "filter 3s, opacity 6s"
                  :filter (str "brightness(" (:brightness @state) ")")}}
    [:img {:src "alixcleanbedwithshadow.png"
           :width "100%"}]]
   [attribution]])

(defn overlay []
  [:div {:style {:content "\"\""
                 :position "absolute"
                 :top "0"
                 :left "0"
                 :right "0"
                 :bottom "0"
                 :transition "background 6s"
                 :background (str "rgba(0, 0, 0, " (- 1 (:brightness @state)) ")")
                 :pointer-events "none"}}])

(defn mobile-background []
  [:div {:style {:position "absolute"
                 :top 0
                 :left 0
                 :height "100%"
                 :width "100%"
                 :background-size "cover"
                 :background-position "right"
                 :background-image "url(Shaw_A_1-788x1024.jpg)"
                 :transition "opacity 6s"
                 :opacity (if (#{:habibula :weave} (:frame @state))
                            "30%" "0%")}}])

(defn background []
  [:div {:style {:position "absolute"
                 :top 0
                 :left 0
                 :height "100%"
                 :width "100%"
                 :background-size "cover"
                 :background-position "bottom"
                 :background-image "url(alixcutoutcleaner-empty.png)"
                 :transition "opacity 6s"
                 :opacity (if (#{:habibula :weave} (:frame @state))
                            "100%" "0%")}}])

(defn artist-statement []
  [:div {:style {:margin 45
                 :font-family "courier"
                 :text-align :justify}}
   (str  "Two tapestries, handwoven into digital code. One to cover, one to float above. "
         "The code, a shifting and archival consciousness. "
         "The writing, a work in which the words are never mine. "
         "The performance, an experiment. To sleep beneath the auspices of other people’s dreams. "
         "Wanderings to filter through the interstice of sleep. "
         "My sleep, the warp. "
         "Your words, the weft. "
         "The fabric, our collective humanity. ")])

(defn statement-attribution []
  [:div {:style {:margin 45
                 :font-family "courier"
                 :font-size "14px"
                 :text-align "center"
                 :padding-top "80"}}
   "Project created by "
   [:a {:href "https://alixanneshaw.com"
        :style {:color "grey"
                :text-decoration "none"
                :margin 0
                :padding 0}}
    "Alix Anne Shaw"]
   [:br]
   "Website developed by "
   [:a {:href "https://kimo.life"
        :style {:color "grey"
                :text-decoration "none"
                :margin 0
                :padding 0}}
    "Kimo Knowles"]])

(defn modal []
  (let [full-size {:height "100%" :width "100%"}
        fade {:opacity (if (:modal? @state) 1 0)
              :pointer-events (if (:modal? @state) "auto" "none")
              :transition "opacity 6s"}
        container (merge full-size
                         fade
                         {:position "absolute"
                          :z-index 9
                          :top 0
                          :left 0})
        horizontal-center (merge full-size
                                 {:display "flex"
                                  :justify-content "center"})
        vertical-center {:display "flex"
                         :flex-direction "column"
                         :justify-content "center"
                         :padding-left "200"
                         :padding-right "200"}
        background (merge full-size
                          {:position "absolute"
                           :background "rgba(0,0,0,0.5)"
                           :top 0
                           :left 0})
        foreground {:z-index 10
                    :position "relative"
                    :color "black"
                    :min-width "10rem"
                    :max-width "28rem"
                    :min-height "30rem"
                    :margin-bottom "90px"
                    :background "white"
                    :margin-left "40px"}]
    [:div {:style container}
     [:div {:style horizontal-center}
      [:div {:style vertical-center}
       [:div {:style background
              :on-click close-modal!}]
       [:div {:style foreground}
        [artist-statement]
        [statement-attribution]]]]]))

(defn mobile-modal []
  (let [full-size {:height "100%" :width "100%"}
        fade {:opacity (if (:modal? @state) 1 0)
              :pointer-events (if (:modal? @state) "auto" "none")
              :transition "opacity 6s"}
        container (merge full-size
                         fade
                         {:position "absolute"
                          :z-index 9
                          :top 0
                          :left 0})
        foreground (merge full-size
                          {:z-index 10
                           :position "relative"
                           :color "black"
                           :background "white"})
        close-button [:div {:style {:width "100%"
                                    :text-align "right"}}
                      [:a {:href "#"
                           :on-click close-modal!
                           :style {:text-decoration "none"
                                   :font-family "Arial, sans-serif"
                                   :color "grey"}}
                       "CLOSE"]]]
    [:div {:style container}
     [:div {:style foreground}
      close-button
      [artist-statement]
      [statement-attribution]]]))

(defn timbers []
  [:div {:style (merge {:display "flex"
                        :width "100%"
                        :height "100%"
                        :overflow "hidden"
                        :margin 0
                        :padding 0
                        :background-color (:background-color @state)
                        :transition "background-color 6s"
                        :color @text-color
                        :justify-content "center"})}
   (if (:mobile-layout? @state)
     [:<>
      [mobile-modal]
      [mobile-background]
      [overlay]
      [:div {:style {:position "relative"}}
       [router]]
      [attribution]]
     [:div {:style {:display "flex"
                    :width "50%"
                    :height "100%"
                    :flex-direction "column"}}
      [modal]
      [background]
      [header]
      [:div {:style {:flex 1
                     :width "100%"
                     :min-height "20rem"}}
       [:div {:style {:min-height "50%"
                      :max-height "80%"
                      :margin-top "4rem"
                      :color @text-color}}
        [overlay]
        [:div {:style {:position "relative"}}
         [router]]]]
      (when-not (= :admin (:frame @state))
        [footer])])])

(defn init-state []
  (when (= "/admin" js/window.location.pathname)
    (swap! state assoc :frame :login)
    (log-in!)))

(defn mount []
  (when-let [el (.getElementById js/document "app")]
    (init-state)
    (rdom/render [timbers] el)))
