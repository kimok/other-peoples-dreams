(ns ashaw.op-dreams
  (:require [clojure.data.json :as json]
            [ring.adapter.jetty :refer [run-jetty]]
            [ring.middleware.file :refer [wrap-file]]
            [ring.middleware.cors :refer [wrap-cors]]
            [ring.middleware.session :refer [wrap-session]]
            [ring.middleware.session.cookie :refer [cookie-store]]
            [muuntaja.middleware :as mtj]
            [postal.core :as postal]
            [next.jdbc :as jdbc]
            [buddy.hashers :as hash]))

(def db-spec {:dbtype "postgresql"
              :dbname "opdreams"
              :user "postgres"
              :host "db"})

(def ds (jdbc/get-datasource db-spec))

(defn q [sql] (jdbc/execute! ds [sql]))

(defn undreameds []
  (q "SELECT * FROM dreams WHERE shared = true;"))

(defn unremembereds []
  (q "SELECT * FROM dreams;"))

(defn forget-dream! [{:keys [id]}]
  (q (str "DELETE FROM dreams WHERE id = " id)))

(defn store-dream! [{:keys [id dream author nightmare recurring shared]
                     :or {author "nobody" nightmare false recurring false shared false}}]
  (if id
    (jdbc/execute! ds [(str "INSERT INTO dreams "
                            "(id, dream, author, nightmare, recurring, shared) "
                            "VALUES (?, ?, ?, ?, ?, ?) "
                            "ON CONFLICT (id) DO UPDATE SET "
                            "dream = EXCLUDED.dream, "
                            "author = EXCLUDED.author, "
                            "nightmare = EXCLUDED.nightmare, "
                            "recurring = EXCLUDED.recurring, "
                            "shared = EXCLUDED.shared")
                       id dream author nightmare recurring shared])
    (jdbc/execute! ds [(str "INSERT INTO dreams "
                            "(dream, author, nightmare, recurring, shared) "
                            "VALUES (?, ?, ?, ?, ?)")
                       dream author nightmare recurring shared])))

(defn authenticated? [{:keys [session]
                       {:keys [password]} :body-params}]
  (or (:authenticated? session)
      (:valid (hash/verify password
                           "bcrypt+sha512$0d280417a634233fde3a5355257c2537$12$a5368847d19b748ff2d6f988614257320f1eb2f95d460f73"))))

(defn notify! []
  {:response (postal/send-message {:host "smtp.gmail.com"
                        :user "ashaw444"
                        :pass (System/getenv "GOOGLE_APP_PASSWORD")
                        :port 587
                        :tls  true}
                       {:from    "ashaw444@gmail.com"
                        :to      "ashaw444@gmail.com"
                        :subject "Hi!"
                        :body    "Test."})
   :test-pass (apply str (take-last 4 (System/getenv "GOOGLE_APP_PASSWORD")))})

(defn router [{:keys [uri request-method body-params] :as request}]
  (case uri
    "/"       {:status  200
               :headers {"Content-Type" "text/html"}
               :body    (java.io.File. "public/index.html")}
    "/dream"  {:status  200
               :headers {"Content-Type" "application/json"}
               :body    (json/write-str
                         (if (= :get request-method)
                           (rand-nth (or (seq (undreameds)) [{:dream "a dreamless sleep."}]))
                           (do
                             (store-dream! body-params)
                             {:stored true
                              :req    (str request)})))}
    "/notify" {:status  200
               :headers {"Content-Type" "application/json"}
               :body    (json/write-str
                         (when (= :post request-method)
                           (when-let [result (notify!)]
                             {:notified true
                              :result   result})))}
    "/forget" {:status  200
               :headers {"Content-Type" "application/json"}
               :body    (json/write-str
                         (do (forget-dream! body-params)
                             {:forgot (:id body-params)}))}
    "/unremembereds"
    {:status  200
     :headers {"Content-Type" "application/json"}
     :body    (json/write-str {:unremembereds (unremembereds)})}
    "/admin"  (if (= :post request-method)
                (do (println request)
                    (let [auth? (authenticated? request)]
                      {:status  200
                       :headers {"Content-type" "application/json"}
                       :body    (json/write-str {:authenticated? auth?})
                       :session {:authenticated? auth?}}))
                {:status  200
                 :headers {"Content-type" "text/html"}
                 :body    (java.io.File. "public/index.html")})))

(def handler (-> router
                 (wrap-cors :access-control-allow-origin  #".*"
                            :access-control-allow-methods [:get :post])
                 (wrap-session {:store (cookie-store)})
                 (wrap-file "public")
                 mtj/wrap-format
                 ))

(defn -main [] (run-jetty handler {:port 8818}))
